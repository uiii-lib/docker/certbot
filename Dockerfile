FROM certbot/certbot

RUN apk add gettext

COPY config.ini.template /opt/certbot/config.ini.template

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "sh", "/entrypoint.sh" ]
