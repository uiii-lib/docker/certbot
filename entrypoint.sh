#!/bin/bash

for var in DOMAINS DOMAIN_EMAIL HTTP_SERVER_CONTAINER_NAME; do
	eval value="\${$var}"
	if [ -z "$value" ]; then
		echo "Missing '$var' environment varibale"
		exit 1
	fi
done

cat config.ini.template | envsubst > config.ini

while ! nc -z ${HTTP_SERVER_CONTAINER_NAME} 80; do
	echo "Waiting HTTP server";
	sleep 10s & wait ${!};
done

certbot certonly -n -c config.ini;

trap exit TERM;

while :; do
	certbot renew;
	sleep 12h & wait ${!};
done;
