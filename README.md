# Certbot docker image

Requires running certbot-friendly http server in other container.

Certbot-friendly means it can handle the path `/.well-known/acme-challenge` route it to a directory which could be linked to `/var/www/certbot` in this container.

## Environment variables

- `DOMAINS` - comma separated list of domains to be certified
- `DOMAIN_EMAIL` - email used in the certificates
- `HTTP_SERVER_CONTAINER_NAME` - name of the http server container
